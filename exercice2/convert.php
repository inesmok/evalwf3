<?php
$euro = null;

if (isset($_POST['validate'])) {

    $euro = isset($_POST['euro']) ? trim($_POST['euro']) : null;

    // Fonction de conversion des euros en dollars.
    // L'opération est euro * taux de change soit 1.1457 pour obtenir en dollars
    function convert($euros,$exchange)
    {
        $euros = $euros*$exchange;
        return $euros;
    }

    $convert = convert($_POST['euro'],1.1457);
}
 ?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>On part en voyage</title>
    <meta name="description" content="">
  </head>
    <body>

        <h3>convertisseur des euros en dollars</h3>

        <form method="POST" name="convertEurUsd">
            <input type="text" name="euro" id="valeur" placeholder="Entrez une valeur en euros.">
            <input type="submit" name="validate" value="Convertir">
        </form>

        <!-- Affichage du resultat de la conversion -->
        <?php
        global $convert;
        if (isset($convert)){
        echo $euro."€ = ".$convert."$";
        }
        ?>
    </body>
</html>
