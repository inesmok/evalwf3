<?php

// Je déclare les champs de mon tableau
$coordonnees = array (
    'Firstname' => 'ines',
    'Lastname'  => 'mokaddem',
    'Address'   => '10 place des reignaux',
    'Postcode'  => '59800',
    'City'      => 'Lille',
    'Email'     => 'ines.mokaddem@gmail.com',
    'Phone'    => '0625222511',
    'Birthday'  => '30-05-1991',
);

?>
<!DOCTYPE html>
    <html>
        <head>
            <title>Présentation</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        </head>
        <body>

                <h2>Présentation</h2>
                    <p>Voici un tableau de présentation :</p>
                    <!-- Boucle pour afficher le contenu de mon tableau dans une liste -->
                    <ul>

                    <!-- Boucle du tableau des informations dans une liste HTML -->
                    <?php foreach ($coordonnees as $line => $value) : ?>
                        <li>
                            <?php if( $line == "Birthday") {
                                $date = new DateTime($value);
                                echo $line." : ". $date->format('d/m/Y');
                            } else {
                                echo $line." : ". $value;
                            } ?>
                        </li>
                <?php endforeach; ?> <!-- fin de la boucle -->
                </ul>

        </body>
    </html>
