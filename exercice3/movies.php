<?php
include_once 'init.php';

// Definition des variables par défaut
$title          = null;
$actors         = null;
$director       = null;
$producer       = null;
$year_of_prod   = null;
$language       = null;
$category       = null;
$storyline      = null;
$video          = null;


// On controle si l'utilisateur envois le formulaire d'inscription
if (isset($_POST['sendfilm'])){

    // Récupération des données du formulaire

    $title        = isset($_POST['title']) ? $_POST['title'] : null;
    $actors       = isset($_POST['actors']) ? $_POST['actors'] : null;
    $director     = isset($_POST['director']) ? $_POST['director'] : null;
    $producer     = isset($_POST['producer']) ? $_POST['producer'] : null;
    $year_of_prod = isset($_POST['year_of_prod']) ? $_POST['year_of_prod'] : null;
    $language     = isset($_POST['language  ']) ? $_POST['language  '] : null;
    $category     = isset($_POST['category']) ? $_POST['category'] : null;
    $storyline    = isset($_POST['storyline']) ? $_POST['storyline'] : null;
    $video        = isset($_POST['video']) ? $_POST['video'] : null;



    // Peut on enregistrer les données du formulaire dans la BDD ?
    // Par défaut : OUI
    // On surchargera avec la valeur FALSE, dans le cas ou le controle du
    // formulaire détecte une erreur.
    $send = true;

    // - Controle du title
    // --

    if (strlen($title) < 5) {
    $send = false;
    echo ("Le champ titre doit contenir au minimum 5 caractères.");
    }
    elseif (strlen($actors) < 5) {
        $send = false;
        echo ("Le champ acteur doit contenir au minimum 5 caractères.");
    }
    elseif (strlen($director) < 5) {
            $send = false;
            echo ("Le champ directeur doit contenir au minimum 5 caractères.");
    }
    elseif (strlen($producer) < 5) {
            $send = false;
            echo ("Le champ producteur doit contenir au minimum 5 caractères.");
    }
    elseif (strlen($storyline) < 5) {
            $send = false;
            echo ("Le champ synopsis doit contenir au minimum 5 caractères.");
    }



    if ($send) {
        // Enregistrement du film
        $q = "INSERT INTO `movies` (`title`,`actors`, `director`,`producer`,`year_of_prod`,`language`,`category`, `storyline`, `video`) VALUES (:title, :actors, :director, :producer, :year_of_prod, :language, :category, :storyline, :video)";
        $q = $pdo->prepare($q);
        $q->bindValue(":title", $title, PDO::PARAM_STR);
        $q->bindValue(":actors", $actors, PDO::PARAM_STR);
        $q->bindValue(":director", $director, PDO::PARAM_STR);
        $q->bindValue(":producer", $producer, PDO::PARAM_STR);
        $q->bindValue(":year_of_prod", $year_of_prod, PDO::PARAM_INT);
        $q->bindValue(":language", $language, PDO::PARAM_STR);
        $q->bindValue(":category", $category, PDO::PARAM_STR);
        $q->bindValue(":storyline", $storyline, PDO::PARAM_STR);
        $q->bindValue(":video", $video, PDO::PARAM_STR);



    }
}
else {

    // On génère le Token
    // Le Token servira à vérifier l'intégrité du formulaire
    $_SESSION['token'] = md5(uniqid());

}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>MOVIES</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">

            <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">

                <h3>Add a new movie</h3>

                <form class="form-horizontal" method="POST">

                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title" name="title" placeholder="5 caractères minimum" value="<?php echo $title; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "title");
                                }
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="actors" class="col-sm-2 control-label">Actors</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="" name="actors" placeholder="5 caractères minimum" value="<?php echo $actors; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "actors");
                                }
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="director" class="col-sm-2 control-label">Director</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="director" name="director" placeholder="5 caractères minimum" value="<?php echo $director; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "director");
                                }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="producer" class="col-sm-2 control-label">Producer</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="producer" name="producer" placeholder="5 caractères minimum" value="<?php echo $producer; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "producer");
                                }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="year_of_prod" class="col-sm-2 control-label">year of production</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="year_of_prod" name="year_of_prod" placeholder="required" value="<?php echo $year_of_prod; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "year_of_prod");
                                }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="language" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                            <select id="category" name="category" placeholder="required" value="<?php echo $category; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "category");
                                }
                            ?>
                            <option value="Science-fiction">Science-fiction</option>
                            <option value="Romance">Romance</option>
                            <option value="Policier">Policier</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-2 control-label">Language</label>
                        <div class="col-sm-10">
                            <select id="category" name="language" value="<?php echo $language; ?>">

                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "category");
                                }
                            ?>
                            <option value="French">French</option>
                            <option value="English">English</option>
                            <option value="Spanish">Spanish</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="storyline" class="col-sm-2 control-label">Storyline</label>
                        <div class="col-sm-10">
                            <textarea id="storyline" name="storyline" placeholder="5 caractères minimum" value="<?php echo $storyline; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "storyline");
                                }
                            ?>
                        </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="video" class="col-sm-2 control-label">Video</label>
                        <div class="col-sm-10">
                            <input type="url" class="form-control" id="video" name="video" placeholder="URL required" value="<?php echo $video; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "video");
                                }
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="sendfilm" class="btn btn-default">Valider</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </body>
</html>
