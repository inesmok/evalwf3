<?php

$title  = null;
$actors   = null;
$director      = null;
$producer   = null;
$year_of_prod   = null;
$language   = null;
$category   = null;
$storyline   = null;
$video   = null;


function formatString ($string) {
    $string = trim($string);
    $string = addslashes($string);
    return $string;
}

function printError ($error, $field) {
    foreach ($error as $data) {
         if ( $data['field'] == $field ) {
             return $data['message'];
         }
    }
}

// On detecte l'envois du formulaire via la methode POST
if (!empty($_POST)) {

    // Récupération des données de $_POST
    $title  = formatString($_POST['title']);
    $actors  = formatString($_POST['actors']);
    $director    = formatString($_POST['director']);
    $producer   = formatString($_POST['producer']);
    $year_of_prod  = formatString($_POST['year_of_prod']);
    $language  = formatString($_POST['language']);
    $category   = formatString($_POST['category']);
    $storyline  = formatString($_POST['storyline']);
    $video   = formatString($_POST['video']);



    // Création du tableau d'erreur
    $error = [];

    // le titre, le directeur, les acteurs, les producteurs et la synopsis doit contenir au minimum 5 caractères
    if (strlen($title) < 5) {
        array_push($error, array(
            "field" => " ",
            "message" => "Doit contenir au minimum 5 caractères"
        ));
    }
    if (strlen($director) < 5) {
        array_push($error, array(
            "field" => " ",
            "message" => "Doit contenir au minimum 5 caractères"
        ));
    }
    if (strlen($actors) < 5) {
            array_push($error, array(
                "field" => " ",
                "message" => "Doit contenir au minimum 5 caractères"
        ));
    }
    if (strlen($producer) < 5) {
            array_push($error, array(
                "field" => " ",
                "message" => "Doit contenir au minimum 5 caractères"
                ));
    }
    if (strlen($storyline) < 5) {
            array_push($error, array(
                "field" => " ",
                "message" => "Doit contenir au minimum 5 caractères"
        ));
    }
    // La langue et la category sont des champs obligatoires

    // Enregistrement du film dans la base de données
    if (empty($error)) {
        echo "ON ENREGISTRE EN BDD.";
    }

}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Mon formulaire</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>
    <body>

        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">

                <h3>Add a new movie</h3>

                <form class="form-horizontal" method="POST">

                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title" name="title" placeholder="5 caractères minimum" value="<?php echo $title; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "title");
                                }
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">Actors</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="" name="actors" placeholder="5 caractères minimum" value="<?php echo $actors; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "actors");
                                }
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Director</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="director" name="director" placeholder="5 caractères minimum" value="<?php echo $director; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "director");
                                }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Producer</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="producer" name="producer" placeholder="5 caractères minimum" value="<?php echo $producer; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "producer");
                                }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">year of production</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="year_of_prod" name="year_of_prod" placeholder="required" value="<?php echo $year_of_prod; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "year_of_prod");
                                }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Language</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="language" name="language" placeholder="required" value="<?php echo $language; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "language");
                                }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="category" name="category" placeholder="required" value="<?php echo $category; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "category");
                                }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Storyline</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="storyline" name="storyline" placeholder="5 caractères minimum" value="<?php echo $storyline; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "storyline");
                                }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Video</label>
                        <div class="col-sm-10">
                            <input type="url" class="form-control" id="video" name="video" placeholder="URL required" value="<?php echo $video; ?>">
                            <?php
                                // On determine si la variable $error existe
                                if (isset($error)) {
                                    echo printError($error, "video");
                                }
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Valider</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </body>
</html>
